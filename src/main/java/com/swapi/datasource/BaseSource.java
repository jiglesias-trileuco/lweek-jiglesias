package com.swapi.datasource;

public interface BaseSource {
    <T> T getFromId(String uri);
}
