package com.swapi.datasource.person;

import com.swapi.datasource.BaseSource;
import com.swapi.receivedmodel.ReceivedPerson;

import java.util.Set;

public interface PersonDataSource extends BaseSource {
    Set<ReceivedPerson> getPeopleByName(String name);
}
