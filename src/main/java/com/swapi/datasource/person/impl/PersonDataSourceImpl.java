package com.swapi.datasource.person.impl;

import com.swapi.datasource.person.PersonDataSource;
import com.swapi.receivedmodel.ReceivedPeople;
import com.swapi.receivedmodel.ReceivedPerson;
import com.swapi.utils.Mappings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.util.Set;

@Component
public class PersonDataSourceImpl implements PersonDataSource {

    @Autowired
    private RestTemplate restTemplate;
    private ReceivedPeople peopleResponse;

    @Override
    public Set<ReceivedPerson> getPeopleByName(String name) {
         peopleResponse = restTemplate.getForObject(Mappings.BASE_URL + Mappings.PATH_PERSON_RESOURCE_BY_NAME,
                ReceivedPeople.class,
                name);
        Set<ReceivedPerson> personSet = peopleResponse.getResults();
        boolean hasNext = StringUtils.isEmpty(peopleResponse.getNext());
        while (!hasNext) {
            hasNext = hasNextPeople(personSet);
        }
        return personSet;
    }

    private boolean hasNextPeople(Set<ReceivedPerson> personSet) {
        peopleResponse = getNextPeople(peopleResponse.getNext());
        personSet.addAll(peopleResponse.getResults());
        return StringUtils.isEmpty(peopleResponse.getNext());
    }

    private ReceivedPeople getNextPeople(String url) {
        return restTemplate.getForObject(url, ReceivedPeople.class);
    }

    //TODO
    @Override
    public <T> T getFromId(String uri) {
        return null;
    }
}
