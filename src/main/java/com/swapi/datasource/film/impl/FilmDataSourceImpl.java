package com.swapi.datasource.film.impl;

import com.swapi.datasource.film.FilmDataSource;
import com.swapi.model.Film;
import com.swapi.utils.Mappings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class FilmDataSourceImpl implements FilmDataSource {
    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Film getFromId(String id) {
        return restTemplate.getForObject(Mappings.BASE_URL + Mappings.PATH_FILM_RESOURCE_BY_ID,
                Film.class,
                id);
    }
}
