package com.swapi.datasource.film;

import com.swapi.datasource.BaseSource;
import com.swapi.model.Film;

public interface FilmDataSource extends BaseSource {
    Film getFromId(String id);
}
