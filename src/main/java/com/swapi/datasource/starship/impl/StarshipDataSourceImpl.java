package com.swapi.datasource.starship.impl;

import com.swapi.datasource.starship.StarshipDataSource;
import com.swapi.model.Starship;
import com.swapi.utils.Mappings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class StarshipDataSourceImpl implements StarshipDataSource {
    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Starship getFromId(String id) {
        return restTemplate.getForObject(Mappings.BASE_URL + Mappings.PATH_STARSHIP_RESOURCE_BY_ID,
                Starship.class,
                id);
    }
}
