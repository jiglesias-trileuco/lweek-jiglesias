package com.swapi.datasource.starship;

import com.swapi.datasource.BaseSource;
import com.swapi.model.DriveableMachine;

public interface StarshipDataSource extends BaseSource {
    DriveableMachine getFromId(String id);
}
