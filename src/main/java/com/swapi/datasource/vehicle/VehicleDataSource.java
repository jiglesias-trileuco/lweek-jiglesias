package com.swapi.datasource.vehicle;

import com.swapi.datasource.BaseSource;
import com.swapi.model.DriveableMachine;

public interface VehicleDataSource extends BaseSource {
    DriveableMachine getFromId(String id);
}
