package com.swapi.datasource.vehicle.impl;

import com.swapi.datasource.vehicle.VehicleDataSource;
import com.swapi.model.Vehicle;
import com.swapi.utils.Mappings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class VehicleDataSourceImpl implements VehicleDataSource {
    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Vehicle getFromId(String id) {
        return restTemplate.getForObject(Mappings.BASE_URL + Mappings.PATH_VEHICLE_RESOURCE_BY_ID,
                Vehicle.class,
                id);
    }
}
