package com.swapi.datasource.planet;

import com.swapi.datasource.BaseSource;
import com.swapi.model.Planet;

public interface PlanetDataSource extends BaseSource {
    Planet getFromId(String id);
}
