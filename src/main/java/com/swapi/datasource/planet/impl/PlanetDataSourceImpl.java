package com.swapi.datasource.planet.impl;

import com.swapi.datasource.planet.PlanetDataSource;
import com.swapi.model.Planet;
import com.swapi.utils.Mappings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class PlanetDataSourceImpl implements PlanetDataSource {
    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Planet getFromId(String id) {
        return restTemplate.getForEntity(Mappings.BASE_URL + Mappings.PATH_PLANET_RESOURCE_BY_ID,
                Planet.class,
                id).getBody();
    }
}
