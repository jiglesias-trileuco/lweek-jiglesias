package com.swapi.model;

public class Vehicle extends DriveableMachine {

    public Vehicle(String name, Integer speed) {
        super(name, speed);
    }
    public Vehicle() {
        super();
    }
}
