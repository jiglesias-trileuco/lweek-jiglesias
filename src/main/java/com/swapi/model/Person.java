package com.swapi.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Set;

@Getter
@EqualsAndHashCode
public class Person {
    private String name;
    private String birthYear;
    private String gender;
    private String homeworld;
    private String fastestVehicle;
    private Set<Film> filmList;

    public static class PersonBuilder {
        private String name;
        private String birthYear;
        private String gender;
        private String homeworld;
        private String fastestVehicle;
        private Set<Film> filmList;

        public PersonBuilder(String name) {
            this.name = name;
        }

        public PersonBuilder bornAtYear(String birthYear) {
            this.birthYear = birthYear;

            return this;
        }

        public PersonBuilder withGender(String gender) {
            this.gender = gender;

            return this;
        }

        public PersonBuilder originPlanet(String homeworld) {
            this.homeworld = homeworld;

            return this;
        }

        public PersonBuilder fastestVehicleDriven(String fastestVehicle) {
            this.fastestVehicle = fastestVehicle;

            return this;
        }

        public PersonBuilder beenInFilms(Set<Film> filmList) {
            this.filmList = filmList;

            return this;
        }

        public Person build() {
            Person person = new Person();
            person.name = this.name;
            person.birthYear = this.birthYear;
            person.gender = this.gender;
            person.homeworld = this.homeworld;
            person.fastestVehicle = this.fastestVehicle;
            person.filmList = this.filmList;

            return person;
        }
    }
    private Person() {

    }
}
