package com.swapi.model;

import lombok.Data;

@Data
public class Planet {
    private String name;
}
