package com.swapi.model;

public class Starship extends DriveableMachine {

    public Starship(String name, Integer speed) {
        super(name, speed);
    }

    public Starship() {
        super();
    }
}
