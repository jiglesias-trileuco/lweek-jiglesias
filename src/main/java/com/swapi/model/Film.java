package com.swapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Film {
    @JsonProperty("title")
    private String title;
    @JsonProperty("release_date")
    private String releaseDate;
}
