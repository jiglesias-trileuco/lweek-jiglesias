package com.swapi.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class DriveableMachine {

    @JsonProperty("name")
    private String name;

    @JsonProperty("max_atmosphering_speed")
    private Integer speed;

    public void setSpeed(String speed) {
        if (speed.contains("unknown"))
            this.speed = 0;
        else
            this.speed = Objects.requireNonNullElse(Integer.parseInt(speed), 0);
    }
}
