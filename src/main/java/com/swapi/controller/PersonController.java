package com.swapi.controller;

import com.swapi.model.Person;
import com.swapi.service.person.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
public class PersonController {
    @Autowired
    private PersonService personService;

    @GetMapping("/person-info")
    public ResponseEntity<Set<Person>> getPeopleByName(String name) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return ResponseEntity.ok()
                .headers(headers)
                .body(personService.getPeopleByName(name));
    }
}

