package com.swapi.converter;

import com.swapi.config.AsyncConfiguration;
import com.swapi.model.Film;
import com.swapi.service.film.FilmService;
import com.swapi.utils.Asyncronization;
import com.swapi.utils.UriShortener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Future;

@Component
public class FilmConverter {

    @Autowired
    private FilmService filmService;

    public Set<Film> getFilms(Set<String> filmUris) {
        CompletionService<Film> completionService = new ExecutorCompletionService<>(new AsyncConfiguration().asyncExecutor());
        Set<Future<Film>> futureFilms = Asyncronization.getFuturesFromUri(UriShortener.shortenAllToGetIds(filmUris), completionService, filmService);
        return Asyncronization.pullFutureObjects(completionService, futureFilms);
    }
}
