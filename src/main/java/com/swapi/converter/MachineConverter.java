package com.swapi.converter;

import com.swapi.config.AsyncConfiguration;
import com.swapi.model.DriveableMachine;
import com.swapi.model.Vehicle;
import com.swapi.service.EntityService;
import com.swapi.service.starship.StarshipService;
import com.swapi.service.vechile.VehicleService;
import com.swapi.utils.Asyncronization;
import com.swapi.utils.UriShortener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.Set;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Future;

@Component
public class MachineConverter {

    @Autowired
    private VehicleService vehicleService;

    @Autowired
    private StarshipService starshipService;

    public DriveableMachine getFastestVehicle(Set<String> vehiclesUrl, Set<String> starshipsUrl) {
        return getMachinesDriven(vehiclesUrl, starshipsUrl).stream()
                .max(Comparator.comparing(DriveableMachine::getSpeed))
                .orElseGet(() ->new Vehicle("No vehicle driven", 0));
    }

    private Set<DriveableMachine> getMachinesDriven(Set<String> vehiclesUrl, Set<String> starshipsUrl) {
        Set<DriveableMachine> drivenMachines = getMachinesFromUri(vehiclesUrl, vehicleService);
        drivenMachines.addAll(getMachinesFromUri(starshipsUrl, starshipService));

        return drivenMachines;
    }

    private Set<DriveableMachine> getMachinesFromUri(Set<String> machineUris, EntityService service) {
        CompletionService<DriveableMachine> completionService = new ExecutorCompletionService<>(new AsyncConfiguration().asyncExecutor());
        Set<Future<DriveableMachine>> futureMachines = Asyncronization.getFuturesFromUri(UriShortener.shortenAllToGetIds(machineUris), completionService, service);
        return Asyncronization.pullFutureObjects(completionService, futureMachines);
    }
}
