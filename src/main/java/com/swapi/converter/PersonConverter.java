package com.swapi.converter;

import com.swapi.model.Film;
import com.swapi.model.Person;
import com.swapi.receivedmodel.ReceivedPerson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

//Helper class to convert people
@Component
public class PersonConverter {

    @Autowired
    private PlanetConverter planetConverter;
    @Autowired
    private MachineConverter machineConverter;
    @Autowired
    private FilmConverter filmConverter;

    public Person convert(ReceivedPerson receivedPerson) {
        Set<Film> filmSet = filmConverter.getFilms(receivedPerson.getFilmList());
        String homeworldName = planetConverter.convert(receivedPerson.getPlanetUrl()).getName();
        String fastestVehicleDriven = machineConverter.getFastestVehicle(receivedPerson.getVehiclesUrl(), receivedPerson.getStarshipsUrl()).getName();

         return new Person.PersonBuilder(receivedPerson.getName())
                 .bornAtYear(receivedPerson.getBirthYear())
                 .withGender(receivedPerson.getGender())
                 .originPlanet(homeworldName)
                 .beenInFilms(filmSet)
                 .fastestVehicleDriven(fastestVehicleDriven)
                 .build();
    }
}
