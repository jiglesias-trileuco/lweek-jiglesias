package com.swapi.converter;

import com.swapi.model.Planet;
import com.swapi.service.planet.PlanetService;
import com.swapi.utils.UriShortener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PlanetConverter implements Converter<String, Planet> {

    @Autowired
    private PlanetService planetService;

    @Override
    public Planet convert(String uri) {
        return planetService.getFromId(UriShortener.shortenToGetId(uri));
    }
}
