package com.swapi.service;

public interface EntityService {
    <T> T getFromId(String path);
}