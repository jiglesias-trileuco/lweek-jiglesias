package com.swapi.service.planet.impl;

import com.swapi.datasource.planet.PlanetDataSource;
import com.swapi.model.Planet;
import com.swapi.service.planet.PlanetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class PlanetServiceImpl implements PlanetService {
    @Autowired
    private PlanetDataSource planetDataSource;

    @Override
    @Cacheable("planetary")
    public Planet getFromId(String id) {
        return planetDataSource.getFromId(id);
    }
}
