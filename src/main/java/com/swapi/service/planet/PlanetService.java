package com.swapi.service.planet;

import com.swapi.model.Planet;
import com.swapi.service.EntityService;

public interface PlanetService extends EntityService {
    Planet getFromId(String id);
}
