package com.swapi.service.film.impl;

import com.swapi.datasource.film.FilmDataSource;
import com.swapi.model.Film;
import com.swapi.service.film.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class FilmServiceImpl implements FilmService {

    @Autowired
    private FilmDataSource filmDataSource;

    @Override
    @Cacheable(value = "filmography", key = "#id")
    public Film getFromId(String id) {
        return filmDataSource.getFromId(id);
    }
}
