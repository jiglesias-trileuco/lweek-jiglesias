package com.swapi.service.film;

import com.swapi.model.Film;
import com.swapi.service.EntityService;

public interface FilmService extends EntityService {
    Film getFromId(String id);
}
