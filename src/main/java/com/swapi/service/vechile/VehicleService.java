package com.swapi.service.vechile;

import com.swapi.model.DriveableMachine;
import com.swapi.service.EntityService;

public interface VehicleService extends EntityService {
    DriveableMachine getFromId(String id);
}
