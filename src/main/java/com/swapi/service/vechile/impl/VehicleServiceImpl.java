package com.swapi.service.vechile.impl;

import com.swapi.datasource.vehicle.VehicleDataSource;
import com.swapi.model.Vehicle;
import com.swapi.service.vechile.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class VehicleServiceImpl implements VehicleService {
    @Autowired
    private VehicleDataSource vehicleDataSource;

    @Override
    @Cacheable("vehicles")
    public Vehicle getFromId(String id) {
        return (Vehicle) vehicleDataSource.getFromId(id);
    }
}
