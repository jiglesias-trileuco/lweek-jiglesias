package com.swapi.service.person.impl;

import com.swapi.config.AsyncConfiguration;
import com.swapi.converter.PersonConverter;
import com.swapi.datasource.person.PersonDataSource;
import com.swapi.model.Person;
import com.swapi.receivedmodel.ReceivedPerson;
import com.swapi.service.person.PersonService;
import com.swapi.utils.Asyncronization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Future;

@Service
public class PersonServiceImpl implements PersonService {
    @Autowired
    private PersonDataSource personDataSource;

    @Autowired
    private PersonConverter peopleConverter;

    private static Logger log = LoggerFactory.getLogger(PersonServiceImpl.class);

    @Override
    @Cacheable(value = "people", key = "#name")
    public Set<Person> getPeopleByName(String name) {
        log.info("Getting people from SWAPI. Looking at name '{}'", name);
        Set<ReceivedPerson> receivedPeople = personDataSource.getPeopleByName(name);
        return peopleConversion(receivedPeople);
    }

    //region private methods
   private Set<Person> peopleConversion(Set<ReceivedPerson> peopleToConvert) {
       CompletionService<Person> completionService = new ExecutorCompletionService<>(new AsyncConfiguration().asyncExecutor());
       Set<Future<Person>> futures = new HashSet<>();
       for (ReceivedPerson receivedPerson : peopleToConvert) {
           futures.add(completionService.submit(() -> peopleConverter.convert(receivedPerson)));
       }
       return Asyncronization.pullFutureObjects(completionService, futures);
   }

    @Override
    public Person getFromId(String path) {
        return personDataSource.getFromId(path);
    }
    //endregion
}
