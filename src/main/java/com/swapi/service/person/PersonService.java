package com.swapi.service.person;

import com.swapi.model.Person;
import com.swapi.service.EntityService;

import java.util.Set;

public interface PersonService extends EntityService {
    Set<Person> getPeopleByName(String name);
}
