package com.swapi.service.starship.impl;

import com.swapi.datasource.starship.StarshipDataSource;
import com.swapi.model.Starship;
import com.swapi.service.starship.StarshipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class StarshipServiceImpl implements StarshipService {
    @Autowired
    private StarshipDataSource starshipDataSource;

    @Override
    @Cacheable("fleet")
    public Starship getFromId(String id) {
        return (Starship) starshipDataSource.getFromId(id);
    }
}
