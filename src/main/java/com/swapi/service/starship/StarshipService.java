package com.swapi.service.starship;

import com.swapi.model.DriveableMachine;
import com.swapi.service.EntityService;

public interface StarshipService extends EntityService {
    DriveableMachine getFromId(String id);
}
