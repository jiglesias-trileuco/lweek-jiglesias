package com.swapi.errors;

import java.util.concurrent.ExecutionException;

public class AsyncronizeException extends ExecutionException {
    public AsyncronizeException(String ex)
    {
        super(ex + "entity cannot be added. Asyncronization task went wrong");
    }
}
