package com.swapi.utils;

import java.util.HashSet;
import java.util.Set;

public class UriShortener {
    public static String shortenToGetId(String uri) {
        return uri.substring(uri.lastIndexOf('/', uri.lastIndexOf('/') - 1 )).substring(1);
    }

    public static Set<String> shortenAllToGetIds(Set<String> uriSet) {
        Set<String> idSet = new HashSet<>();
        for (String uri: uriSet) {
            idSet.add(shortenToGetId(uri));
        }
        return idSet;
    }
}