package com.swapi.utils;

import com.swapi.errors.AsyncronizeException;
import com.swapi.service.EntityService;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Component
public class Asyncronization {
    public static <T> Set<Future<T>> getFuturesFromUri(Set<String> uris, CompletionService<T> completionService, EntityService service) {
        Set<Future<T>> futures = new HashSet<>();
        for (String uri : uris) {
            futures.add(completionService.submit(() -> service.getFromId(uri)));
        }
        return futures;
    }

    public static <T> Set<T> pullFutureObjects(CompletionService<T> completionService, Set<Future<T>> futures) {
        Set<T> entitySet = new HashSet<>();
        while (futures.size() > 0) {
            Future<T> future = completionService.poll();
            if (future != null) {
                futures.remove(future);
                entitySet.add(tryToGetObject(future));
            }
        }
        return entitySet;
    }

    private static <T> T tryToGetObject(Future<T> future) {
        try {
            return future.get();
        } catch (InterruptedException | ExecutionException e) {
            try {
                throw new AsyncronizeException(e.getMessage());
            } catch (AsyncronizeException ex) {
                ex.printStackTrace();
                return null;
            }
        }
    }
}
