package com.swapi.utils;

public final class Mappings {
    //Endpoints to models
    public static final String PATH_FILM_RESOURCES = "/films";
    public static final String PATH_FILM_RESOURCE_BY_ID = "/films/{id}";
    public static final String PATH_PERSON_RESOURCES = "/people";
    public static final String PATH_PERSON_RESOURCE_BY_ID = "/people/{id}";
    public static final String PATH_PERSON_RESOURCE_BY_NAME = "/people?search={name}";
    public static final String PATH_PLANET_RESOURCES = "/planets";
    public static final String PATH_PLANET_RESOURCE_BY_ID = "/planets/{id}";
    public static final String PATH_SPECIES_RESOURCES = "/species";
    public static final String PATH_SPECIES_RESOURCE_BY_ID = "/species/{id}";
    public static final String PATH_STARSHIP_RESOURCES = "/starships";
    public static final String PATH_STARSHIP_RESOURCE_BY_ID = "/starships/{id}";
    public static final String PATH_VEHICLE_RESOURCES = "/vehicles";
    public static final String PATH_VEHICLE_RESOURCE_BY_ID = "/vehicles/{id}";

    //API SOURCES
    public static final String BASE_URL = "https://swapi.co/api";
}
