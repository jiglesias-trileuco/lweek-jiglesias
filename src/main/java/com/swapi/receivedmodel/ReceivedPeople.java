package com.swapi.receivedmodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReceivedPeople {
    private Set<ReceivedPerson> results;
    private String next;
}
