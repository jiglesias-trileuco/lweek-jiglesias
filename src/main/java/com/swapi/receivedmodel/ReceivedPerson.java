package com.swapi.receivedmodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Set;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReceivedPerson {
    @JsonProperty("name")
    private String name;

    @JsonProperty("birth_year")
    private String birthYear;

    @JsonProperty("gender")
    private String gender;

    @JsonProperty("homeworld")
    private String planetUrl;

    @JsonProperty("vehicles")
    private Set<String> vehiclesUrl;

    @JsonProperty("starships")
    private Set<String> starshipsUrl;

    @JsonProperty("films")
    private Set<String> filmList;
}
