Project which manipulates and handles SWAPI. SWAPI, The Star Wars API it's
a set of data to play with. It presents information about People, Films, Species,
Starships, Vehicles and Planets. This projects works as a client of people info,
and uses it to represent useful data.

SWAPI works with pages and URIs in its results. URIs are mapped "entities" that you
can request from. So, if you want to know a film name from a specific character,
you must do a new request to the URI-Entity film which this character has been.


How this client works?
- This client removes its pagination making subsequent get request to their next
pages and unifies it in a single call. Manage to show relevant info about entities
(films, fastest vehicle driven and homeworld) avoiding a new request.
- Makes multithread calls to improve response times.
- Cache handling.
- Character learning(?). Stores every character in Runtime to answer faster on next
calls and avoiding SWAPI bottleneck.

How to use it?
- After running it, you can do a request to:
       GET http://localhost:8080/swapi-proxy/person-info?name={name}
   where {name} it's a sequence of letters contained in a character name.
   Example:
    - http://localhost:8080/swapi-proxy/person-info?name=Darth
   Search every single character who has a Darth in their names


FAQS
- Im doing a GET request to characters that they have an 'A' on their names. Why
it's getting so long to get a reponse?
    - A single call to SWAPI takes around 2-3 seconds. It exist a bottleneck
    when this client needs to access to the data provided by another API.
- Ok, so why it gets as long as 40 seconds to answer?
    - Every character result has atleast 2 URIs to get after the first call (a
    film and its homeworld).
- SO WHY IT TAKES 40 SECONDS?
    -  Luke Skywalker has a homeworld, five films which he appears, two vehicles
    and two starships driven. We need to make TEN request after the initial one
    to show his character info. That's one character from sixty, AFTER
    DOING A REQUEST SEARCHING 'A' NAMING CHARACTERS.
- Ugh, ok.. I get it. Are you planning to expand functionality on GET resources?
    - Not in your lifetime.
    
    
    
                            -Ok, it can be.